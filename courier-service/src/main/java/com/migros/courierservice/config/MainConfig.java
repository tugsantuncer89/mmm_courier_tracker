package com.migros.courierservice.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

@RefreshScope
@Getter
@Setter
@Configuration
public class MainConfig {

    @Value("${rule.distance}")
    private Integer distance;
}
