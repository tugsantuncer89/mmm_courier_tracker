package com.migros.courierservice.controller;

import com.migros.courierservice.config.MainConfig;
import com.migros.courierservice.dto.NearestStoreDto;
import com.migros.courierservice.dto.StoreLogDto;
import com.migros.courierservice.dto.StoreLogViewModel;
import com.migros.courierservice.entity.Courier;
import com.migros.courierservice.mapper.StoreLogMapper;
import com.migros.courierservice.service.CourierService;
import io.swagger.annotations.Api;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/courier")
@Api("Courier Controller")
public class CourierController {


    @Autowired
    private CourierService courierService;

    @Autowired
    private MainConfig mainConfig;

    @Autowired
    private StoreLogMapper storeLogMapper;

    private static final Logger logger = LogManager.getLogger(CourierController.class);


    @GetMapping("/get-total-travel-distance")
    public ResponseEntity<Double> getTotalTravelDistance(@RequestParam("courierId") long courierId) {
        logger.info("CourierController.getTotalTravelDistance start.");
        try {
            return ResponseEntity
                    .ok(courierService.getTotalTravelDistance(courierId));
        } catch (Exception e) {
            logger.error(e);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .build();
        }

    }


    @PostMapping(path = "/add-courier-travel", consumes = "application/json", produces = "application/json")
    public ResponseEntity<List<StoreLogViewModel>> addCourierTravel(@RequestBody List<Courier> courierTravelList) {
        logger.info("CourierController.addCourierTravel start process.");
        courierService.deleteStoreLogs();
        List<StoreLogDto> response = new ArrayList<>();
        for (Courier item : courierTravelList) {
            try {
                courierService.save(item);
                NearestStoreDto nearestStoreDto = courierService.getNearestStore(item);
                if (nearestStoreDto == null) continue;

                if (checkNearestStore(nearestStoreDto)) {
                    StoreLogDto storeLogDto = courierService.getStoreLog(nearestStoreDto.getName(), item.getCourierId(), item.getTime());
                    if (storeLogDto == null) continue;

                    long seconds = this.getSecondsDifference(item.getTime(), storeLogDto.getTimestamp());

                    if (seconds >= 60) {
                        logger.info("CourierController.addCourierTravel courier entry store storeName={},courierId={},time={}",
                                nearestStoreDto.getName(),
                                item.getCourierId(),
                                item.getTime());
                        courierService.addStoreLog(nearestStoreDto.getName(), item.getCourierId(), item.getTime())
                                .ifPresent(response::add);
                    }
                }
            } catch (Exception e) {
                logger.error(e);
            }

        }
        return ResponseEntity
                .ok(storeLogMapper.convertList(response));
    }

    private boolean checkNearestStore(NearestStoreDto nearestStoreDto) {
        logger.info("CourierController.checkNearestStore nearestStoreDto={}", nearestStoreDto.toString());
        return nearestStoreDto.getDistance() <= mainConfig.getDistance();
    }

    private long getSecondsDifference(long t1, long t2) {
        logger.info("CourierController.getMinutesDifference t1={},t2={}", t1, t2);
        return Math.abs(t1 - t2) / 1000;
    }


}
