package com.migros.courierservice.service;

import com.migros.courierservice.dto.NearestStoreDto;
import com.migros.courierservice.dto.StoreLogDto;
import com.migros.courierservice.entity.Courier;
import com.migros.courierservice.repository.ICourierRepository;
import com.migros.mmm_core.utils.DistanceCalculateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class CourierService implements ICourierService {

    @Autowired
    private ICourierRepository courierRepository;

    @Autowired
    private RestTemplate restTemplate;


    @Override
    public Double getTotalTravelDistance(Long courierId) throws IOException {
        Double totalDistance = 0D;

        List<Courier> courierTravelList = this.getById(courierId);

        if (courierTravelList.size() <= 1)
            return 0D;


        for (int i = 0; i < courierTravelList.size() - 1; i++) {
            var travel1 = courierTravelList.get(i);
            var travel2 = courierTravelList.get(i + 1);

            double distance = DistanceCalculateUtils.distance(travel1.getLat(), travel1.getLng(), travel2.getLat(), travel2.getLng());
            totalDistance += distance;
        }

        return totalDistance;
    }


    public StoreLogDto getStoreLog(String storeName, long courierId, long timestamp) {
        StoreLogDto storeLogDto = Optional.ofNullable(restTemplate.
                        getForEntity("http://STORE-SERVICE/store/log/get-last-log?courierId=" + courierId + "&storeName=" +
                                storeName + "", StoreLogDto.class)
                        .getBody())
                .orElse(null);

        if (null == storeLogDto) {
            return addStoreLog(storeName, courierId, timestamp).orElse(null);
        }
        return storeLogDto;
    }

    public Optional<StoreLogDto> addStoreLog(String storeName, long courierId, long timestamp) {
        StoreLogDto build = StoreLogDto.builder()
                .storeName(storeName)
                .courierId(courierId)
                .timestamp(timestamp).build();
        Optional<StoreLogDto> data = Optional.ofNullable(restTemplate.
                postForEntity("http://STORE-SERVICE/store/log/add", build, StoreLogDto.class)
                .getBody());
        return data;
    }

    public NearestStoreDto getNearestStore(Courier item) {
        NearestStoreDto nearestStoreDto = Optional.ofNullable(restTemplate.
                        getForEntity("http://STORE-SERVICE/store/get-nearest-store?lng=" + item.getLng() + "&lat=" + item.getLat() + "",
                                NearestStoreDto.class).getBody())
                .orElse(null);

        if (null == nearestStoreDto)
            return null;
        return nearestStoreDto;
    }

    @Override
    public Courier save(Courier item) throws Exception {
        return courierRepository.save(item);
    }

    @Override
    public List<Courier> getAll() throws IOException {
        return courierRepository.getAll();
    }

    @Override
    public List<Courier> getById(long courierId) throws IOException {
        return courierRepository.getById(courierId);
    }

    @Override
    public void deleteStoreLogs() {
        restTemplate.
                getForEntity("http://STORE-SERVICE/store/log/delete", void.class);

    }
}
