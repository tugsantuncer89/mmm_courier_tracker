package com.migros.courierservice.service;

import com.migros.courierservice.dto.NearestStoreDto;
import com.migros.courierservice.dto.StoreLogDto;
import com.migros.courierservice.entity.Courier;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ICourierService {
    Double getTotalTravelDistance(Long courierId) throws IOException;

    StoreLogDto getStoreLog(String storeName, long courierId, long timestamp);

    Optional<StoreLogDto> addStoreLog(String storeName, long courierId, long timestamp);

    NearestStoreDto getNearestStore(Courier item);

    Courier save(Courier item) throws Exception;

    List<Courier> getAll() throws IOException;

    List<Courier> getById(long courierId) throws IOException;

    void deleteStoreLogs();
}
