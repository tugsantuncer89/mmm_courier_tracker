package com.migros.courierservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@ToString
public class Courier implements Serializable {
    private long time;
    private long courierId;
    private double lat;
    private double lng;

    public static CourierBuilder builder() {
        return new CourierBuilder();
    }

    public static class CourierBuilder {

        private long time;
        private long courierId;
        private double lat;
        private double lng;


        public CourierBuilder time(final long time) {
            this.time = time;
            return this;
        }

        public CourierBuilder courierId(final long courierId) {
            this.courierId = courierId;
            return this;
        }

        public CourierBuilder lat(final double lat) {
            this.lat = lat;
            return this;
        }

        public CourierBuilder lng(final double lng) {
            this.lng = lng;
            return this;
        }

        public Courier build() {
            return new Courier(time, courierId, lat, lng);
        }

    }
}
