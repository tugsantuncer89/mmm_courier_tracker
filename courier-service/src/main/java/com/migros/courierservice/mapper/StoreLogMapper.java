package com.migros.courierservice.mapper;

import com.migros.courierservice.dto.StoreLogDto;
import com.migros.courierservice.dto.StoreLogViewModel;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
public class StoreLogMapper {
    public StoreLogViewModel convertEntity(StoreLogDto storeLogDto) {
        StoreLogViewModel storeLogViewModel = new StoreLogViewModel();
        storeLogViewModel.setStoreName(storeLogDto.getStoreName());
        storeLogViewModel.setCourierId(storeLogDto.getCourierId());
        Timestamp t1 = new Timestamp(storeLogDto.getTimestamp());
        storeLogViewModel.setDate(t1.toLocalDateTime().format(DateTimeFormatter.ISO_DATE_TIME));
        return storeLogViewModel;
    }

    public List<StoreLogViewModel> convertList(List<StoreLogDto> storeLogDto) {
        List<StoreLogViewModel> list = new ArrayList<>();
        storeLogDto.forEach(item -> {
            list.add(this.convertEntity(item));
        });
        return list;
    }
}
