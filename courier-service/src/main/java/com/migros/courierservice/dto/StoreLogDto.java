package com.migros.courierservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StoreLogDto implements Serializable {
    private String storeName;
    private long courierId;
    private long timestamp;

    public static StoreLogDtoBuilder builder() {
        return new StoreLogDtoBuilder();
    }

    public static class StoreLogDtoBuilder {

        private String storeName;
        private long courierId;
        private long timestamp;

        public StoreLogDtoBuilder storeName(final String storeName) {
            this.storeName = storeName;
            return this;
        }

        public StoreLogDtoBuilder courierId(final long courierId) {
            this.courierId = courierId;
            return this;
        }

        public StoreLogDtoBuilder timestamp(final long timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public StoreLogDto build() {
            return new StoreLogDto(storeName, courierId, timestamp);
        }

    }
}
