package com.migros.courierservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreLogViewModel {

    private String date;
    private String storeName;
    private long courierId;
}
