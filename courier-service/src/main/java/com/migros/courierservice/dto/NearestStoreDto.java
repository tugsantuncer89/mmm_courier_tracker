package com.migros.courierservice.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class NearestStoreDto implements Serializable  {

    private String name;
    private Double distance;

    public static NearestStoreDtoBuilder builder() {
        return new NearestStoreDtoBuilder();
    }

    public static class NearestStoreDtoBuilder {

        private String name;
        private Double distance;

        public NearestStoreDtoBuilder name(final String name) {
            this.name = name;
            return this;
        }

        public NearestStoreDtoBuilder distance(final Double distance) {
            this.distance = distance;
            return this;
        }

        public NearestStoreDto build() {
            return new NearestStoreDto(name, distance);
        }

    }
}
