package com.migros.courierservice.repository;

import com.migros.courierservice.entity.Courier;

import java.io.IOException;
import java.util.List;

public interface ICourierRepository {
    Courier save(Courier item) throws Exception;

    List<Courier> getAll() throws IOException;

    List<Courier> getById(long courierId) throws IOException;
}
