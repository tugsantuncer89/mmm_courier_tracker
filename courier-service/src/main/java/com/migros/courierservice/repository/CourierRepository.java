package com.migros.courierservice.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.migros.courierservice.core.Constants;
import com.migros.courierservice.entity.Courier;
import com.migros.courierservice.mapper.ObjectMappers;
import com.migros.mmm_core.utils.FilePathUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CourierRepository implements ICourierRepository {

    @Autowired
    private FilePathUtils filePathUtils;

    @Autowired
    private ObjectMappers<Courier> objectMappers;

    private static final Logger logger = LogManager.getLogger(CourierRepository.class);

    public Courier save(Courier item) throws Exception {

        if (null == item)
            throw new Exception("Courier item is null!");

        List<Courier> list = this.getAll();
        list.add(item);

        filePathUtils.deleteContent(Constants.COURIER_PATH);

        String entityToJson = objectMappers.getEntityToJson(list);
        filePathUtils.writeContent(Constants.COURIER_PATH, entityToJson);
        logger.info("CourierRepository.save item={}", item.toString());
        return item;
    }


    public List<Courier> getAll() throws IOException {
        String data = filePathUtils.readFileToString(Constants.COURIER_PATH);
        if (data.length() > 0)
            return objectMappers.getJsonToEntity(data, new TypeReference<List<Courier>>() {
            });

        return new ArrayList<Courier>();
    }


    public List<Courier> getById(long courierId) throws IOException {
        logger.info("CourierRepository.getById courierId={}", courierId);
        String data = filePathUtils.readFileToString(Constants.COURIER_PATH);
        List<Courier> result = objectMappers.getJsonToEntity(data, new TypeReference<List<Courier>>() {
        });
        return result.stream()
                .filter(q -> q.getCourierId() == courierId)
                .collect(Collectors.toList());

    }

}
