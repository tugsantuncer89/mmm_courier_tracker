package com.migros.courierservice;

import com.migros.courierservice.core.Constants;
import com.migros.mmm_core.utils.FilePathUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class CourierServiceApplication implements ApplicationRunner {


    @Autowired
    private FilePathUtils filePathUtils;

    public static void main(String[] args) {
        SpringApplication.run(CourierServiceApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        filePathUtils.deleteContent(Constants.COURIER_PATH);
    }
}
