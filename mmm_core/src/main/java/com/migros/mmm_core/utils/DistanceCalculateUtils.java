package com.migros.mmm_core.utils;


public class DistanceCalculateUtils {

    private final static double earthRadius = 6371;// Approximate radius of the earth in kilometers

    /**
     * Get the distance in kilometers between two coordinates.
     * @param lat1 Latitude of the first coordinate, in degrees
     * @param long1 Longitude of the first coordinate, in degrees
     * @param lat2 Latitude of the second coordinate, in degrees
     * @param long2 Longitude of the second coordinate, in degrees
     * @return Distance in kilometers
     */
    public static double distance(double lat1, double long1, double lat2, double long2) {
        double distance = Math.acos(Math.sin(lat2 * Math.PI / 180.0) * Math.sin(lat1 * Math.PI / 180.0) +
                Math.cos(lat2 * Math.PI / 180.0) * Math.cos(lat1 * Math.PI / 180.0) *
                        Math.cos((long1 - long2) * Math.PI / 180.0)) * earthRadius;
        return distance * 1000;
    }


}

