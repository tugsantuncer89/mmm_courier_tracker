package com.migros.mmm_core.utils;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;


@Component
public class FilePathUtils {

    public String readFileToString(String fileName) throws IOException {
        File resource = new ClassPathResource(fileName).getFile();
        return new String(Files.readAllBytes(resource.toPath()));
    }

    public boolean writeContent(String fileName,String data) {
        try {
            File resource = new ClassPathResource(fileName).getFile();
            FileOutputStream writer = new FileOutputStream(resource);
            writer.write(data.getBytes(StandardCharsets.UTF_8));
            writer.close();
        } catch (IOException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public boolean deleteContent(String fileName) {
        try {
            File resource = new ClassPathResource(fileName).getFile();
            FileOutputStream writer = new FileOutputStream(resource);
            writer.write(("").getBytes());
            writer.close();
        } catch (IOException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

    public boolean test() {
       return true;
    }
}
