package com.migros.mmm_core.config;

import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;


public class BaseSwaggerConfig {
	private final String basePackage;

	public BaseSwaggerConfig(String basePackage) {
		this.basePackage = basePackage;
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("migros-api")
				.select()
				.apis(RequestHandlerSelectors.basePackage(basePackage))
				.build()
				.apiInfo(apiInfo());
	}


	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Migros API")
				.description("Migros Store API reference for developers")
				.termsOfServiceUrl("http://javainuse.com")
				.contact("tugsantuncer1989@gmail.com").license("Migros License")
				.licenseUrl("tugsantuncer1989@gmail.com").version("1.0").build();
	}

}