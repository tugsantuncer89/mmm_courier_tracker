package com.migros.mmm_core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmmCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmmCoreApplication.class, args);
	}

}
