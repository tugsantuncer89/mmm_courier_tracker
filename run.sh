#!/bin/bash

set -e

cd `dirname $0`
r=`pwd`
echo $r

# mmm_core library
cd $r/mmm_core
echo "Starting mmm_core library packaging..."
mvn -q clean install -U &&

sleep 10S

# Eureka
cd $r/service-registry
echo "Starting Eureka Service..."
mvn -q clean spring-boot:run &

sleep 10S

# Config Center
cd $r/config-center
echo "Starting Config Center..."
mvn -q clean spring-boot:run &

sleep 10S

# Api Gateway
cd $r/api-gateway
echo "Starting Api Gateway..."
mvn -q clean spring-boot:run &

sleep 10S

# Store Service
cd $r/store-service
echo "Starting Store Service..."
mvn -q clean install -U spring-boot:run &

sleep 10S

# Courier Service
cd $r/courier-service
echo "Starting Courier Service..."
mvn -q clean install -U  spring-boot:run &