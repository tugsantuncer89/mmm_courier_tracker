package com.migros.storeservice;

import com.migros.mmm_core.utils.FilePathUtils;
import com.migros.storeservice.core.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class StoreServiceApplication implements ApplicationRunner {

    @Autowired
    private FilePathUtils filePathUtils;

    public static void main(String[] args) {
        SpringApplication.run(StoreServiceApplication.class, args);
    }
    @Override
    public void run(ApplicationArguments args) throws Exception {
        filePathUtils.deleteContent(Constants.STORE_LOG_PATH);
    }
}
