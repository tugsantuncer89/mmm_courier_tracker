package com.migros.storeservice.config;

import com.migros.mmm_core.utils.FilePathUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig {

    @Bean
    FilePathUtils filePathUtils(){
        return new FilePathUtils();
    }
}
