package com.migros.storeservice.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@RefreshScope
@Getter
@Setter
@Component
public class TestConfig {

    @Value("${my.test}")
    private String value;
}
