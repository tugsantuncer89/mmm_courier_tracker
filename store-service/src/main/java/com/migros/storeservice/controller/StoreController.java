package com.migros.storeservice.controller;

import com.migros.storeservice.dto.NearestStoreDto;
import com.migros.storeservice.entity.Store;
import com.migros.storeservice.service.IStoreService;
import io.swagger.annotations.Api;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping("/store")
@Api("Store Controller")
public class StoreController {

    @Autowired
    private IStoreService storeService;

    private final static Logger logger = LogManager.getLogger(StoreController.class);

    @GetMapping("/list")
    public ResponseEntity<List<Store>> index() {
        try {
            logger.info("get store lists");
            return ResponseEntity.ok(storeService.getStoreList());
        } catch (IOException e) {
            logger.error(e);
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/get-nearest-store")
    public ResponseEntity<NearestStoreDto> getNearestStore(@RequestParam("lng") double lng,
                                           @RequestParam("lat") double lat) {
        try {
            logger.info("get store lists");
            return ResponseEntity.ok(storeService.getNearestStore(lng, lat));
        } catch (IOException e) {
            logger.error(e);
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
