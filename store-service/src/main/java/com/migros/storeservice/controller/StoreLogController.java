package com.migros.storeservice.controller;

import com.migros.storeservice.entity.StoreLog;
import com.migros.storeservice.service.IStoreLogService;
import io.swagger.annotations.Api;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/store/log")
@Api("Store Log Controller")
public class StoreLogController {

    @Autowired
    private IStoreLogService storeLogService;

    private final static Logger logger = LogManager.getLogger(StoreLogController.class);

    @PostMapping(path = "/add", consumes = "application/json", produces = "application/json")
    public StoreLog addLog(@RequestBody StoreLog item) {
        try {
            logger.info("StoreLogController.addLog item={}", item.toString());
            return storeLogService.save(item);
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    @GetMapping("/get-last-log")
    public StoreLog getLastLog(@RequestParam("courierId") long courierId,
                               @RequestParam("storeName") String storeName) {
        try {
            logger.info("StoreLogController.getLastLog courierId={} storeName={}", courierId, storeName);
            return storeLogService.getLastStoreLogByCourierIdAndStoreName(courierId, storeName);
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    @GetMapping("/delete")
    public void delete() {
        try {
            logger.info("StoreLogController.delete store_log.json file");
            storeLogService.delete();
        } catch (Exception e) {
            logger.error(e);
        }
    }
}
