package com.migros.storeservice.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ObjectMappers<T> {

    @Autowired
    private ObjectMapper objectMapper;

    public List<T> getJsonToEntity(String source, TypeReference<List<T>> type) throws JsonProcessingException {
        return objectMapper.readValue(source, type);
    }

    public String getEntityToJson(List<T> type) throws JsonProcessingException {
        return objectMapper.writeValueAsString(type);
    }

}
