package com.migros.storeservice.entity;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class Store {
    private String name;
    private double lat;
    private double lng;

    public static StoreBuilder builder() {
        return new StoreBuilder();
    }

    public static class StoreBuilder {
        private String name;
        private double lat;
        private double lng;


        public StoreBuilder name(final String name) {
            this.name = name;
            return this;
        }

        public StoreBuilder courierId(final double lat) {
            this.lat = lat;
            return this;
        }

        public StoreBuilder timestamp(final double lng) {
            this.lng = lng;
            return this;
        }


        public Store build() {
            return new Store(name, lat, lng);
        }

    }
}
