package com.migros.storeservice.entity;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StoreLog implements Serializable {
    private String storeName;
    private long courierId;
    private long timestamp;

    public static StoreLogBuilder builder() {
        return new StoreLogBuilder();
    }

    public static class StoreLogBuilder {

        private String storeName;
        private long courierId;
        private long timestamp;


        public StoreLogBuilder storeName(final String storeName) {
            this.storeName = storeName;
            return this;
        }

        public StoreLogBuilder courierId(final long courierId) {
            this.courierId = courierId;
            return this;
        }

        public StoreLogBuilder timestamp(final long timestamp) {
            this.timestamp = timestamp;
            return this;
        }


        public StoreLog build() {
            return new StoreLog(storeName, courierId, timestamp);
        }

    }
}
