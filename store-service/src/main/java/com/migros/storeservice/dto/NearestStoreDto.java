package com.migros.storeservice.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NearestStoreDto {

    private String name;
    private double distance;

    public static NearestStoreDtoBuilder builder() {
        return new NearestStoreDtoBuilder();
    }

    public static class NearestStoreDtoBuilder {

        private String name;
        private double distance;

        public NearestStoreDtoBuilder name(final String name) {
            this.name = name;
            return this;
        }

        public NearestStoreDtoBuilder courierId(final double distance) {
            this.distance = distance;
            return this;
        }


        public NearestStoreDto build() {
            return new NearestStoreDto(name, distance);
        }

    }
}
