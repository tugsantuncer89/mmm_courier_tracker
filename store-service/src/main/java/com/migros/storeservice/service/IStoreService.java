package com.migros.storeservice.service;

import com.migros.storeservice.dto.NearestStoreDto;
import com.migros.storeservice.entity.Store;

import java.io.IOException;
import java.util.List;


public interface IStoreService {
    List<Store> getStoreList() throws IOException;

    NearestStoreDto getNearestStore(double lng, double lat) throws IOException;
}
