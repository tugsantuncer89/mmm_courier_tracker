package com.migros.storeservice.service;

import com.migros.storeservice.entity.StoreLog;
import com.migros.storeservice.repository.IStoreLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

@Service
public class StoreLogService implements IStoreLogService {

    @Autowired
    private IStoreLogRepository storeLogRepository;

    @Override
    public StoreLog save(StoreLog item) throws Exception {
        return storeLogRepository.save(item);
    }

    @Override
    public List<StoreLog> getAll() throws IOException {
        return storeLogRepository.getAll();
    }


    public StoreLog getLastStoreLogByCourierIdAndStoreName(long courierId, String storeName) throws IOException {
        return this.getAll().stream()
                .filter(q -> q.getCourierId() == courierId && q.getStoreName().equals(storeName))
                .max(Comparator.comparing(StoreLog::getTimestamp))
                .orElse(null);
    }

    @Override
    public void delete() {
        storeLogRepository.delete();
    }
}
