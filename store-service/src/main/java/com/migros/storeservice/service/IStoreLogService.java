package com.migros.storeservice.service;

import com.migros.storeservice.entity.StoreLog;

import java.io.IOException;
import java.util.List;

public interface IStoreLogService {
    StoreLog save(StoreLog item) throws Exception;

    List<StoreLog> getAll() throws IOException;

    StoreLog getLastStoreLogByCourierIdAndStoreName(long courierId, String storeName) throws IOException;

    void delete();
}
