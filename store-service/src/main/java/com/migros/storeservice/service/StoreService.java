package com.migros.storeservice.service;

import com.migros.mmm_core.utils.DistanceCalculateUtils;
import com.migros.storeservice.dto.NearestStoreDto;
import com.migros.storeservice.entity.Store;
import com.migros.storeservice.repository.IStoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class StoreService implements IStoreService {

    @Autowired
    private IStoreRepository storeRepository;

    @Override
    public List<Store> getStoreList() throws IOException {
        return storeRepository.getStoreList();
    }

    @Override
    public NearestStoreDto getNearestStore(double lng, double lat) throws IOException {

        List<Store> storeList = this.getStoreList();
        List<NearestStoreDto> list = new ArrayList<>();

        for (Store store : storeList) {
            double distance = DistanceCalculateUtils.distance(lat, lng, store.getLat(), store.getLng());
            NearestStoreDto entity = NearestStoreDto.builder()
                    .name(store.getName())
                    .distance(distance)
                    .build();
            list.add(entity);
        }

        return list.stream()
                .min(Comparator.comparing(NearestStoreDto::getDistance))
                .orElse(null);

    }


}
