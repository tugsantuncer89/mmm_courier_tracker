package com.migros.storeservice.core;

public class Constants {
    public final static String STORE_JSON_PATH = "/static/stores.json";
    public final static String STORE_LOG_PATH = "/static/store_logs.json";
}
