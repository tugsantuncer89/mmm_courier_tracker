package com.migros.storeservice.repository;

import com.migros.storeservice.entity.StoreLog;

import java.io.IOException;
import java.util.List;

public interface IStoreLogRepository {
    StoreLog save(StoreLog item) throws Exception;

    List<StoreLog> getAll() throws IOException;

   void delete();
}
