package com.migros.storeservice.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.migros.mmm_core.utils.FilePathUtils;
import com.migros.storeservice.core.Constants;
import com.migros.storeservice.entity.StoreLog;
import com.migros.storeservice.mapper.ObjectMappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StoreLogRepository implements IStoreLogRepository {

    @Autowired
    private FilePathUtils filePathUtils;

    @Autowired
    private ObjectMappers<StoreLog> objectMappers;


    @Override
    public StoreLog save(StoreLog item) throws Exception {
        if (null == item)
            throw new Exception("storelog item is null!");

        List<StoreLog> list = this.getAll();
        list.add(item);

        filePathUtils.deleteContent(Constants.STORE_LOG_PATH);

        String entityToJson = objectMappers.getEntityToJson(list);
        filePathUtils.writeContent(Constants.STORE_LOG_PATH, entityToJson);

        return item;
    }

    public List<StoreLog> getAll() throws IOException {
        String data = filePathUtils.readFileToString(Constants.STORE_LOG_PATH);
        if (data.length() > 0)
            return objectMappers.getJsonToEntity(data, new TypeReference<List<StoreLog>>() {
            });

        return new ArrayList<StoreLog>();
    }

    @Override
    public void delete() {
        filePathUtils.deleteContent(Constants.STORE_LOG_PATH);
    }

}
