package com.migros.storeservice.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.migros.mmm_core.utils.FilePathUtils;
import com.migros.storeservice.core.Constants;
import com.migros.storeservice.entity.Store;
import com.migros.storeservice.mapper.ObjectMappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.List;

@Repository
public class StoreRepository implements IStoreRepository {

    @Autowired
    private FilePathUtils filePathUtils;

    @Autowired
    private ObjectMappers<Store> objectMappers;

    @Override
    public List<Store> getStoreList() throws IOException {
        String text = filePathUtils.readFileToString(Constants.STORE_JSON_PATH);
        return objectMappers.getJsonToEntity(text, new TypeReference<List<Store>>() {
        });
    }


}
