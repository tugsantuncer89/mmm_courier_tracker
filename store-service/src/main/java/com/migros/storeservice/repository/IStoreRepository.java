package com.migros.storeservice.repository;

import com.migros.storeservice.entity.Store;

import java.io.IOException;
import java.util.List;


public interface IStoreRepository {
    List<Store> getStoreList() throws IOException;
}
