
# Migros Courier Tracker Case



## Installation

run migros courier tracker with bash

```bash
  cd CURRENT_DIR_REPO/
  bash run.sh
```
    
## API Reference

#### Get all apis

http://localhost:8081/swagger-ui.html

http://localhost:8082/swagger-ui.html

#### Add Courier Travel

```http
  POST http://localhost:8080/courier/add-courier-travel
```

| Body |

[
    {
        "time": 1632829303093,
        "courierId": 1,
        "lat": 40.99202003456955,
        "lng": 29.12047403330337
    },
    {
        "time": 1632829308093,
        "courierId": 1,
        "lat": 40.992146488485425,
        "lng": 29.12216801747486
    }
]

#### Get Total Travel Distance By Courier

```http
  GET http://localhost:8080/courier/get-total-travel-distance?courierId={courierId}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `courierId`      | `long` | **Required**. Id of courier |






  
## Authors

- [tugsantuncer89](https://www.gitlab.com/tugsantuncer89)

  